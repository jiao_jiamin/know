const devBaseURL = "";
const proBaseURL = "http://120.76.55.53:8080";
export const BASE_URL = process.env.NODE_ENV === 'development' ? devBaseURL: proBaseURL;

export const TIMEOUT = 5000;
