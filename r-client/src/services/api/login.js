import request from "../request.js";

// 登录接口
export const LOGIN = (data = null) => request('POST', '/api/login', data)