import request from "../request.js";

export const UPLOAD = (data, config) => request("POST", '/api/upload/file', data, config)

export const MERGE = (data) => request("POST", '/api/upload/merge', data)

export const CHECK = (data) => request("POST", '/api/upload/check', data)