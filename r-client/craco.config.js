const path = require('path')
const resolve = dir => path.resolve(__dirname, dir)
const webpack = require('webpack')
const CompressionWebpackPlugin = require("compression-webpack-plugin")
const isPro = process.env.NODE_ENV === 'production'

module.exports = {
  webpack: {
    alias: {
      '@': resolve('src'),
      "components": resolve("src/components")
    },
    babel: {
      plugins: [
        // 配置 babel-plugin-import
        [
          'import',
          {
            libraryName: 'antd',
            libraryDirectory: 'es',
            style: 'css',
          },
          'antd'
        ]
      ]
    },
    configure: (webpackConfig, { env, paths }) => {
      if (isPro) {
        webpackConfig.devtool = false;
        webpackConfig.optimization.splitChunks = {
          ...webpackConfig.optimization.splitChunks,
          ...{
            chunks: 'all',
            name: true,
          }
        }
        webpackConfig.plugins.push(new CompressionWebpackPlugin({
          test: /\.(css|js)$/i,
          threshold: 0,
          minRatio: 0.8,
          algorithm: "gzip",
        }))
        webpackConfig.plugins.push(new webpack.ContextReplacementPlugin(/moment\/locale$/, /zh-cn/))
      }
      return webpackConfig
    }
  }
}