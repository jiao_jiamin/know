# 实现知乎

- react 客户端
```
1. cd r-client

2. yarn install

3. yarn start
```

- Vue3 客户端
```
1. cd v-client

2. npm install

3. npm run dev
```

- 服务端
```
1. cd server

2. npm install / yarn 

3. npm start
```

# 项目预览: [http://120.76.55.53:8080](http://120.76.55.53:8080)
- 账号：admin
- 密码：123456

